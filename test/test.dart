import 'package:flutter/foundation.dart';

class PubScpec {
  String version;
  int b;

  PubScpec(this.version, {this.b});

  printVersion() {
    print(version);
  }

  @override
  String toString() {
    return 'PubScpec{version: $version, b: $b}';
  }
}

main() {
  Set<int> a2 = {};
  Set<int> a3 = Set();
  a2.add(1);
  print(a2);

  Map<String, int> al = {'adf': 23};
  al['adf'] = 23;
  print(al['adf']);
  print(al);

  PubScpec p = PubScpec("Priv", b: 12);
  p.printVersion();

  Map<Object, Object> response = {"status":"ok","text":"text","table":"webdebug"};
}
