import 'package:calculator/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('test show app', (WidgetTester tester) async {
    // отрисовка виджета
    await tester.pumpWidget(MaterialApp(
      home: MyHomePage(
        helloText: "test title",
      ),
    ));

    //проверка
    expect(find.text('строковый литерал'), findsOneWidget);
    expect(find.text('другой текст'), findsNothing);
  });
}
