import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String helloText;

  MyHomePage({Key key, this.helloText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(helloText),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            child: Center(child: Text("Hello")),
            color: Colors.green,
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 100,
                width: 100,
                child: Center(child: Text("Hello")),
                color: Colors.blue,
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          try {
            Response<String> respose = await Dio().post(
              "http://192.168.2.139:8081/table/webdebug/",
              queryParameters: {
                "order": "test",
              },
            );
            var json = jsonDecode(respose.data);
            if ('error' == json['ststus']) {
              print(json['text']);
            }
          } catch (e) {
            print(e);
          }
        },
      ),
    );
  }
}
